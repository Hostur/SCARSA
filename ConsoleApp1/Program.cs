﻿using System;
using System.IO;
using SCARSA;

namespace ConsoleApp1
{
    class Program
    {
        static void Main(string[] args)
        {
            RSAKeyGenerator.GenerateKeys(@"C:\Users\fzielinski\Desktop\keys\");
            var rsa = new RSAServer.RsaServer(File.ReadAllText(@"C:\Users\fzielinski\Desktop\keys\public_key.txt"),
                File.ReadAllText(@"C:\Users\fzielinski\Desktop\keys\private_key.txt"));
            string s = rsa.Encyptr("test");
            string g = rsa.Decrypt(s);
            Console.WriteLine("Hello World!");
        }
    }
}
