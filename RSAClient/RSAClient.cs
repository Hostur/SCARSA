﻿using SCARSA;

namespace RSAClient
{
  public class RsaClient : RSABase
  {
    private readonly string publicKey;

    public RsaClient(string publicKey)
    {
      this.publicKey = publicKey;
    }

    public string EncryptRSAPasswordedHash(string[] content)
    {
      return EncryptRSAPasswordedHash(content, publicKey);
    }

    public string Encyptr(string content)
    {
      return Encrypt(content, publicKey);
    }
  }
}
