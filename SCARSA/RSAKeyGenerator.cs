﻿using System;
using System.IO;
using System.Security.Cryptography;

namespace SCARSA
{
  public static class RSAKeyGenerator
  {
    private const string PRIVATE_KEY = "private_key.txt";
    private const string PUBLIC_KEY = "public_key.txt";

    public static void GenerateKeys(string path)
    {
      var cryptoServiceProvider = new RSACryptoServiceProvider(2048);
      //var privateKey = cryptoServiceProvider.ExportParameters(true);
      //var publicKey = cryptoServiceProvider.ExportParameters(false);

      string privateKeyString = ToXmlStringExt(cryptoServiceProvider, true);
      string publicKeyString = ToXmlStringExt(cryptoServiceProvider, false);

      File.WriteAllText(Path.Combine(path, PUBLIC_KEY), publicKeyString);
      File.WriteAllText(Path.Combine(path, PRIVATE_KEY), privateKeyString);
    }

    //private static string GetKeyString(RSAParameters publicKey)
    //{
    //  using (var stringWriter = new StringWriter())
    //  {
    //    var xmlSerializer = new System.Xml.Serialization.XmlSerializer(typeof(RSAParameters));
    //    xmlSerializer.Serialize(stringWriter, publicKey);
    //    return stringWriter.ToString();
    //  }
    //}

    private static string ToXmlStringExt(RSA rsa, bool includePrivateParameters)
    {
      var parameters = rsa.ExportParameters(includePrivateParameters);

      return string.Format(
          "<RSAKeyValue><Modulus>{0}</Modulus><Exponent>{1}</Exponent><P>{2}</P><Q>{3}</Q><DP>{4}</DP><DQ>{5}</DQ><InverseQ>{6}</InverseQ><D>{7}</D></RSAKeyValue>",
          parameters.Modulus != null ? Convert.ToBase64String(parameters.Modulus) : null,
          parameters.Exponent != null ? Convert.ToBase64String(parameters.Exponent) : null,
          parameters.P != null ? Convert.ToBase64String(parameters.P) : null,
          parameters.Q != null ? Convert.ToBase64String(parameters.Q) : null,
          parameters.DP != null ? Convert.ToBase64String(parameters.DP) : null,
          parameters.DQ != null ? Convert.ToBase64String(parameters.DQ) : null,
          parameters.InverseQ != null ? Convert.ToBase64String(parameters.InverseQ) : null,
          parameters.D != null ? Convert.ToBase64String(parameters.D) : null);
    }
  }
}
