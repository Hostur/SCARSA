﻿using System;
using System.IO;
using System.Security.Cryptography;
using System.Text;
using System.Xml;

namespace SCARSA
{
  public abstract class RSABase
  {
    private readonly string[] PasswordedKeys = { "m@@Re", "S@6&F%Sj", "@1KK2c4e5116g7v4" };
    private string PublicPassword => PasswordedKeys[0];
    private string SaltKey => PasswordedKeys[1];
    private string VIKey => PasswordedKeys[2];
    private const char SEPARATOR = ':';
    private readonly UnicodeEncoding _encoder = new UnicodeEncoding();

    public string[] SplitHash(string hash)
    {
      return hash.Split(SEPARATOR);
    }

    public string EncryptWithPassword(string plainText)
    {
      byte[] plainTextBytes = Encoding.UTF8.GetBytes(plainText);

      byte[] keyBytes = new Rfc2898DeriveBytes(PublicPassword, Encoding.ASCII.GetBytes(SaltKey)).GetBytes(256 / 8);
      var symmetricKey = new RijndaelManaged() { Mode = CipherMode.CBC, Padding = PaddingMode.Zeros };
      var encryptor = symmetricKey.CreateEncryptor(keyBytes, Encoding.ASCII.GetBytes(VIKey));

      byte[] cipherTextBytes;

      using (var memoryStream = new MemoryStream())
      {
        using (var cryptoStream = new CryptoStream(memoryStream, encryptor, CryptoStreamMode.Write))
        {
          cryptoStream.Write(plainTextBytes, 0, plainTextBytes.Length);
          cryptoStream.FlushFinalBlock();
          cipherTextBytes = memoryStream.ToArray();
          cryptoStream.Close();
        }
        memoryStream.Close();
      }
      return Convert.ToBase64String(cipherTextBytes);
    }

    public string DecryptWithPassword(string encryptedText)
    {
      byte[] cipherTextBytes = Convert.FromBase64String(encryptedText);
      byte[] keyBytes = new Rfc2898DeriveBytes(PublicPassword, Encoding.ASCII.GetBytes(SaltKey)).GetBytes(256 / 8);
      var symmetricKey = new RijndaelManaged() { Mode = CipherMode.CBC, Padding = PaddingMode.None };

      var decryptor = symmetricKey.CreateDecryptor(keyBytes, Encoding.ASCII.GetBytes(VIKey));
      var memoryStream = new MemoryStream(cipherTextBytes);
      var cryptoStream = new CryptoStream(memoryStream, decryptor, CryptoStreamMode.Read);
      byte[] plainTextBytes = new byte[cipherTextBytes.Length];

      int decryptedByteCount = cryptoStream.Read(plainTextBytes, 0, plainTextBytes.Length);
      memoryStream.Close();
      cryptoStream.Close();
      return Encoding.UTF8.GetString(plainTextBytes, 0, decryptedByteCount).TrimEnd("\0".ToCharArray());
    }
    

    protected string[] DecryptRSAPasswordedHash(string passwordedRSAHash, string privateKey)
    {
      var passwordedHash = Decrypt(passwordedRSAHash, privateKey);
      var hash = DecryptWithPassword(passwordedHash);
      return SplitHash(hash);
    }

    protected string EncryptRSAPasswordedHash(string[] content, string publicKey)
    {
      var passwordedHash = GetPasswordedHash(content);
      return Encrypt(passwordedHash, publicKey);
    }

    public string GetPasswordedHash(string[] content)
    {
      StringBuilder builder = new StringBuilder(content.Length);
      int i = 0;
      foreach (string value in content)
      {
        i++;
        builder.Append(value);
        if (i < content.Length)
        {
          builder.Append(SEPARATOR);
        }
      }
      return EncryptWithPassword(builder.ToString());
    }

    protected string Encrypt(string textToEncrypt, string publicKeyString)
    {
      var bytesToEncrypt = Encoding.UTF8.GetBytes(textToEncrypt);

      using (var rsa = new RSACryptoServiceProvider(2048))
      {
        try
        {
          FromXmlStringExt(rsa, publicKeyString);
          var encryptedData = rsa.Encrypt(bytesToEncrypt, true);
          var base64Encrypted = Convert.ToBase64String(encryptedData);
          return base64Encrypted;
        }
        finally
        {
          rsa.PersistKeyInCsp = false;
        }
      }
    }

    protected string Decrypt(string textToDecrypt, string privateKeyString)
    {
      using (var rsa = new RSACryptoServiceProvider(2048))
      {
        try
        {             
          FromXmlStringExt(rsa, privateKeyString);

          var resultBytes = Convert.FromBase64String(textToDecrypt);
          var decryptedBytes = rsa.Decrypt(resultBytes, true);
          var decryptedData = Encoding.UTF8.GetString(decryptedBytes);
          return decryptedData;
        }
        finally
        {
          rsa.PersistKeyInCsp = false;
        }
      }
    }

    public void FromXmlStringExt(RSA rsa, string xmlString)
    {
      var parameters = new RSAParameters();

      var xmlDoc = new XmlDocument();
      xmlDoc.LoadXml(xmlString);

      if (xmlDoc.DocumentElement.Name.Equals("RSAKeyValue"))
      {
        foreach (XmlNode node in xmlDoc.DocumentElement.ChildNodes)
        {
          switch (node.Name)
          {
            case "Modulus":
              parameters.Modulus =
                string.IsNullOrEmpty(node.InnerText) ? null : Convert.FromBase64String(node.InnerText);
              break;
            case "Exponent":
              parameters.Exponent =
                string.IsNullOrEmpty(node.InnerText) ? null : Convert.FromBase64String(node.InnerText);
              break;
            case "P":
              parameters.P = string.IsNullOrEmpty(node.InnerText) ? null : Convert.FromBase64String(node.InnerText);
              break;
            case "Q":
              parameters.Q = string.IsNullOrEmpty(node.InnerText) ? null : Convert.FromBase64String(node.InnerText);
              break;
            case "DP":
              parameters.DP = string.IsNullOrEmpty(node.InnerText) ? null : Convert.FromBase64String(node.InnerText);
              break;
            case "DQ":
              parameters.DQ = string.IsNullOrEmpty(node.InnerText) ? null : Convert.FromBase64String(node.InnerText);
              break;
            case "InverseQ":
              parameters.InverseQ =
                string.IsNullOrEmpty(node.InnerText) ? null : Convert.FromBase64String(node.InnerText);
              break;
            case "D":
              parameters.D = string.IsNullOrEmpty(node.InnerText) ? null : Convert.FromBase64String(node.InnerText);
              break;
          }
        }
      }
      else
      {
        throw new Exception("Invalid XML RSA key.");
      }

      rsa.ImportParameters(parameters);
    }
  }
}
