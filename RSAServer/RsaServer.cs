﻿using SCARSA;

namespace RSAServer
{
  public class RsaServer : RSABase
  {
    private readonly string publicKey;
    private readonly string privateKey;

    public RsaServer(string publicKey, string privateKey)
    {
      this.publicKey = publicKey;
      this.privateKey = privateKey;
    }

    public string EncryptRSAPasswordedHash(string[] content)
    {
      return EncryptRSAPasswordedHash(content, publicKey);
    }

    public string Encyptr(string content)
    {
      return Encrypt(content, publicKey);
    }

    public string Decrypt(string content)
    {
      return Decrypt(content, privateKey);
    }

    public string[] DecryptRSAPasswordedHash(string content)
    {
      return DecryptRSAPasswordedHash(content, privateKey);
    }
  }
}
